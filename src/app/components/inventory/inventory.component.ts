import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from 'src/environments/environment';
import * as inventario from '../../../assets/resources/inventory.json';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  @Input() showHeader: boolean = true;
  principalMenu: any[] = [];
  items: MenuItem[] = [];
  user: any;
  AllInventory: any;
  columnName = [
    { field: 'codigo', header: 'Código', style: 'black' },
    { field: 'materia_prima', header: 'Materia prima', style: 'black' },
    { field: 'cantidad', header: 'Cantidad' , style: 'black' },
    { field: 'unidad', header: 'Unidad' , style: 'red'},
    { field: 'ubicacion', header: 'Ubicación', style: 'red' },
    { field: 'proveedor', header: 'Proveedor', style: 'red' },
    { field: 'fecha_de_vencimiento', header: 'Fecha de vencimiento' , style: 'red'},
  ]
  selectedProducts: any = [];
  Delete: string = '';
  inventoryUnfiltered: any;
  messages: any = [];


  constructor(
    private router: Router,
    private orderService: OrderService
  ) {}

  async ngOnInit() {
    this.AllInventory = await this.getInventory();
    console.log('inventory', this.AllInventory);
  }

  async onSearch(event: any) {
    let filter;

    if (event.target.value !== '') {
      filter = this.AllInventory.filter( (item: any) => {
        let condition = item.codigo.toLowerCase().search(event.target.value) !== -1 ||
          item.producto.toLowerCase().search(event.target.value) !== -1 ||
          item.materia_prima.toLowerCase().search(event.target.value) !== -1 ||
          item.codigo.toUpperCase().search(event.target.value) !== -1 ||
          item.producto.toUpperCase().search(event.target.value) !== -1 ||
          item.materia_prima.toUpperCase().search(event.target.value) !== -1;

        return condition ? item : undefined;
      })

      this.AllInventory = filter ? filter : await this.getInventory();
    } else {
      this.AllInventory = await this.getInventory();
      console.log('onSearch', this.AllInventory);
    }

    console.log('onSearch', event.target.value);
  }

  async getInventory () {
    let inventory: any = await this.orderService.handlerInventory();
    return inventory ? inventory?.inventario : undefined;
  }

  save () {
    this.messages = [{ severity: 'success', summary: 'success', detail: 'Se ha guardado separación de productos.' }];
    setTimeout(() => this.messages = [], 1500);
  }

  navigateTo() {
    this.router.navigate(['/dashboard']);
  }

}
