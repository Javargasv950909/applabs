import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/services/order.service';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  formulario: any;
  messages: any = [];
  num_informe: any;
  solicitado: any;
  procedencia: any;
  fecha_recepcion: any;
  fecha_analisis: any;
  fecha_emision: any;
  direccion: any;
  lote: any;
  descripcion: any;
  opcion1: any;
  valoropcion1: any;
  opcion2: any;
  valoropcion2: any;
  opcion3: any;
  valoropcion3: any;
  opcion4: any;
  valoropcion4: any;
  opcion5: any;
  valoropcion5: any;
  opciones: any;
  metodo: any;
  estandar: any;
  observaciones: any;
  metodos: any;
  emitido: any;

  constructor(
    private forms: UntypedFormBuilder,
    private router: Router,
    private orderService: OrderService
  ) { }

  ngOnInit(): void {

    this.opciones = [
      {name: 'Humedad (%p/P)', code: 'Humedad (%p/P)'},
      {name: 'Proteína (%p/P)', code: 'Proteína (%p/P)'},
      {name: 'PH', code: 'PH'},
      {name: 'Cenizas', code: 'Cenizas'},
      {name: 'Almidones', code: 'Almidones'}
    ];

    this.metodos = [
      {name: 'MEF 10-1038', code: 'MEF 10-1038'},
      {name: 'ADCYAA-OO8', code: 'ADCYAA-OO8'},
      {name: 'MEF 10-1059', code: 'MEF 10-1059'},
      {name: 'MEF 10-1067', code: 'MEF 10-1067'},
    ];
  }

  formatDate(date: any) {
    var fechaOriginal = new Date(date);

    function obtenerDiaConCeros(dia: any) {
      return dia < 10 ? '0' + dia : dia;
    }
    var dia = obtenerDiaConCeros(fechaOriginal.getDate());
    var mes = obtenerDiaConCeros(fechaOriginal.getMonth() + 1);
    var anio = fechaOriginal.getFullYear();
    var fechaFormateada = dia + '/' + mes + '/' + anio;
    return fechaFormateada;
  }

  createPdf() {

    this.fecha_recepcion = this.formatDate(this.fecha_recepcion);
    this.fecha_analisis = this.formatDate(this.fecha_analisis);
    this.fecha_emision = this.formatDate(this.fecha_emision);

    const pdfDefinition: any = {
      content : [
        {
          style: 'tableExample',
          color: '#444',
          table: {
            widths: ['auto', 200, 'auto', 'auto', 'auto'],
            body: [
              [{},{text: 'ALPINA PRODUCTOS ALIMENTICIOS S.A.', colSpan: 2, alignment: 'center'},{text: 'ALPINA PRODUCTOS ALIMENTICIOS S.A.', alignment: 'center'},{}, {}],
              [{			
                image: 'snow',
                width: 80,
                height: 50,
                rowSpan: 3,
                alignment: 'center',
                valign: "bottom", align: "right"
              }, {text: 'INFORME DE RESULTADOS LABORATORIO', colSpan: 2, alignment: 'center'}, 'campo3', {text:'Código', alignment: 'center', fontSize: 10}, {text: '2585', alignment: 'center', fontSize: 10}],
              [{}, {text: 'LABORATORIO DE SERVICIOS I+D+i', rowSpan: 2, colSpan: 2, alignment: 'center', margin: [0, 20, 0, 8]}, 'campo7', {text: 'Nivel de revisión', alignment: 'center', fontSize: 10}, {text: 'Alto', alignment: 'center', fontSize: 10}],
              [{}, {}, {}, {text: 'Vigente Desde:', alignment: 'center', fontSize: 10}, {text: '20/02/2024', fontSize: 10, alignment: 'center'}],
            ]
          }
        },
        '\n\n\n',
        {
          alignment: 'justify',
          columns: [
            {
              width: '*',
              style: 'tableExample',
              table: {
                body: [
                  [{text: 'INFORME N°:', fontSize: 10}, this.num_informe ? this.num_informe : ''],
                  [{text: 'SOLICITADO POR:', fontSize: 10}, this.solicitado ? this.solicitado : ''],
                  [{text: 'PROCEDENCIA DE LA MUESTRA:', fontSize: 10}, this.procedencia ? this.procedencia : ''],
                ],
              },
              layout: 'noBorders'
            },
            {
              width: '*',
              style: 'tableExample',
              table: {
                body: [
                  [{text: 'FECHA RECEPCION DE LA MUESTRA:', fontSize: 10}, this.fecha_recepcion ? this.fecha_recepcion : ''],
                  [{text: 'FECHA ANALISIS:', fontSize: 10}, this.fecha_analisis ? this.fecha_analisis : ''],
                  [{text: 'FECHA EMISION RESULTADOS:', fontSize: 10}, this.fecha_emision ? this.fecha_emision : ''],
                  [{text: 'DIRECCIÓN DEL LABORATORIO:', fontSize: 10}, this.direccion ? this.direccion : ''],
                ]
              },
              layout: 'noBorders'
            }
          ]
        },
        '\n\n',
        {
          width: '*',
          table: {
            body: [
              [{text: 'ANALISIS FISICOQUÍMICO/IDENTIFICACIÓN DE LA MUESTRA', colSpan: 2, alignment: 'center', fontSize: 10}, {},{text: this.opcion1.code ? this.opcion1.code : '', rowSpan: 2, alignment: 'center', margin: [0, 20, 0, 8], fontSize: 10}, {text: this.opcion2.code ? this.opcion2.code : '', rowSpan: 2, alignment: 'center', margin: [0, 20, 0, 8], fontSize: 10}, {text: this.opcion3.code ? this.opcion3.code : '', rowSpan: 2, alignment: 'center', margin: [0, 20, 0, 8], fontSize: 10}, {text: this.opcion4.code ? this.opcion4.code : '', rowSpan: 2, alignment: 'center', margin: [0, 20, 0, 8], fontSize: 10}, {text: this.opcion5.code ? this.opcion5.code : '', rowSpan: 2, alignment: 'center', margin: [0, 20, 0, 8], fontSize: 10}],
              [{text: 'LOTE/ ORDEN/ CODIGO DE LA MUESTRA', alignment: 'center', fontSize: 10}, {text: 'DESCRIPCIÓN DE LA MUESTRA', alignment: 'center', fontSize: 10}, {}, {}, {}, {}, {}],
              [this.lote ? this.lote : '', this.descripcion ? this.descripcion : '', this.valoropcion1 ? this.valoropcion1 : '', this.valoropcion2 ? this.valoropcion2 : '', this.valoropcion3 ? this.valoropcion3 : '', this.valoropcion4 ? this.valoropcion4 : '', this.valoropcion5 ? this.valoropcion5 : ''],
            ]
          }
        },
        '\n\n',
        {
          width: '*',
          style: 'tableExample',
          table: {
            body: [
              [{text: 'METODO:', fontSize: 10}, this.metodo.code ? this.metodo.code : ''],
              [{text: 'ESTANDAR  (Cuando Aplique)', fontSize: 10}, this.estandar ? this.estandar : ''],
            ]
          },
        },
        {text: 'OBSERVACIONES:', fontSize: 12, bold: true, margin: [0, 20, 0, 8]},
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*'],
            body: [
              [this.observaciones ? this.observaciones : ''],
            ]
          }
        },
        '\n',
        {text: 'Convenciones: Min.: Mínimo / Max. : Máximo / N.A.: No Aplica', fontSize: 10},
        '\n',
        {text: 'Aclaraciones a tener en cuenta para interpretación del informe:', fontSize: 10},
        {text: '1. Los resultados de este reporte corresponden solo a las muestras (items) sometidos a ensayo.', fontSize: 10},
        {text: '2. El laboratorio no se hace responsable por la información entregada por el cliente y pueda afectar la validez de los resultados. Los resultados se aplican a la muestra como se recibió por parte del cliente.', fontSize: 10},
        '\n\n',
        {
          style: 'tableExample',
          table: {
            widths: [80, 'auto'],
            body: [
              [{text: 'INFORME EMITIDO POR: ', fontSize: 12, bold: true}, {text: this.emitido ? this.emitido : '', fontSize: 12}],
            ]
          },
          layout: 'noBorders'
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: [80, 'auto'],
            body: [
              [{text: 'CARGO: ', fontSize: 12, bold: true}, {text: 'Profesional Laboratorio y Planta Piloto', fontSize: 12}],
            ]
          },
          layout: 'noBorders'
        },
      ],
      images: {
        snow: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Alpina_S.A._logo.svg/2560px-Alpina_S.A._logo.svg.png',
      }
    }

    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();
  }

  submitForm() {
    if (this.formulario.valid) {
      this.messages = [{ severity: 'success', summary: 'success', detail: 'Proyecto creado con éxito' }];
      setTimeout(() => this.messages = [], 1500);
      return this.orderService.handlerCreateProjects(this.formulario.value);
    }

    this.messages = [{ severity: 'error', summary: 'error', detail: 'Hubo un error creando el proyecto.' }];
    setTimeout(() => this.messages = [], 1500);
    return { 'status': false, method: 'submitForm' };
  }

  navigateTo() {
    this.router.navigate(['/dashboard']);
  }

}
